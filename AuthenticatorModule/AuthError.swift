//
//  AuthError.swift
//  AuthenticatorModule
//
//  Created by LinuxPlus on 10/2/17.
//  Copyright © 2017 Allam. All rights reserved.
//

import Foundation

class AuthError: Error {
    var localizedDescription: String
    
    init(error: String) {
        localizedDescription = error
    }
}
