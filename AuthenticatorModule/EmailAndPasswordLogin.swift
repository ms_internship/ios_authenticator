//
//  EmailAndPasswordLogin.swift
//  AuthenticatorModule
//
//  Created by LinuxPlus on 9/6/17.
//  Copyright © 2017 Allam. All rights reserved.
//

import Foundation
import UIKit
import FirebaseAuth

class EmailAndPasswordLogin:NSObject, BaseLoginProtocol{
    var authenticator: LoginAuthProtocol?
    var email:String!
    var password: String!
    let auth = Auth.auth()
     let authManager = AuthenticatorManager.shared
    
    init(email:String, password: String, authenticator: LoginAuthProtocol) {
        super.init()
        self.email = email
        self.password = password
        self.authenticator = authenticator
    }
    
    func login(){
        self.authenticator?.authByEmailAndPassword(email: email, password: password, completion: { (uid, error) in
            guard error == nil else{
                self.authManager.AuthFailed(withError: error!)
                return
            }
            if let uid = uid{
                self.authManager.AuthSuccessfull(uid: uid)
            }
        })
    }
    
    
    
}
