//
//  PhoneNumberLogin.swift
//  AuthenticatorModule
//
//  Created by LinuxPlus on 9/12/17.
//  Copyright © 2017 Allam. All rights reserved.
//

import Foundation
import UIKit
import FirebaseAuth


class PhoneNumberVerification: BasePhoneVerificationProtocol {
    var authenticator: PhoneAuthProtocol?
    var phoneNumber: String!
    var viewController: UIViewController!
    let authManager = AuthenticatorManager.shared
    let codeVerification = PhoneCodeVerification()
    
    
    init(viewController: UIViewController, phoneNumber: String, authenticator: PhoneAuthProtocol){
        self.viewController = viewController
        self.phoneNumber = phoneNumber
        self.authenticator = authenticator
        authManager.dataProvider = PhoneDataProvider()
    }
    
    
    func verify() {
        ActivitySpinner.show("Sending verification code...")
        PhoneAuthProvider.provider().verifyPhoneNumber(phoneNumber) { (verificationID, error) in
            ActivitySpinner.hide()
            if let error = error{
                self.authManager.AuthFailed(withError: AuthError(error: error.localizedDescription))
                return
            }
            
            self.codeVerification.verifySentCode(viewController: self.viewController, resendBlock: {self.verify()})
            { verificationCode in
                ActivitySpinner.show("Verifing number \nplease wait...")
                self.authenticator?.authByPhoneNumber(verificationID: verificationID!, verificationCode: verificationCode){ (uid, error) in
                    if let error = error{
                        self.authManager.AuthFailed(withError: error)
                        return
                    }
                    if let uid = uid{
                        self.authManager.PhoneVerificationSuccessfull(uid: uid)
                    }
                }
                
            }
        }
    }
    
    
}
