//
//  ActivitySpinner.swift
//  AuthenticatorModule
//
//  Created by LinuxPlus on 10/1/17.
//  Copyright © 2017 Allam. All rights reserved.
//

import Foundation
import UIKit
import SwiftSpinner

class ActivitySpinner{
    
    static func show(_ title: String){
        SwiftSpinner.show(title).addTapHandler({
            SwiftSpinner.hide()
        }, subtitle: "Note: Tap to hide this loader.\nwill still sending the request after hide.")
    }
    
    static func hide(){
        SwiftSpinner.hide()
    }
}
