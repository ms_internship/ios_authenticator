//
//  FirebasePhoneAuthenticator.swift
//  AuthenticatorModule
//
//  Created by LinuxPlus on 9/18/17.
//  Copyright © 2017 Allam. All rights reserved.
//

import Foundation
import FirebaseAuth
 

class FirebasePhoneAuthenticator: PhoneAuthProtocol {
        
    func authByPhoneNumber(verificationID: String, verificationCode: String, completion: @escaping (String?, AuthError?) -> Void) {
        let phoneCredintal = PhoneAuthProvider.provider().credential(withVerificationID: verificationID, verificationCode: verificationCode)
        Auth.auth().signIn(with: phoneCredintal) { user, error in
            ActivitySpinner.hide()
            if let error = error{
                completion(nil, AuthError(error: error.localizedDescription))
            }
            if let user = user{
                completion(user.uid, nil)
            }
        }
    }
}
