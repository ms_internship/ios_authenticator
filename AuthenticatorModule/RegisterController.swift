//
//  RegisterController.swift
//  AuthenticatorModule
//
//  Created by LinuxPlus on 9/14/17.
//  Copyright © 2017 Allam. All rights reserved.
//

import Foundation
import UIKit

class RegisterController{
    var authenticator: RegisterAuthProtocol!
    var emailAndPasswordRegister: BaseRegisterProtocol!
    
    
    init() {
        self.authenticator = FirebaseRegisterAuthenticator()
    }
    
    func emailAndPasswordRegister(withUser user: User) {
            emailAndPasswordRegister = EmailAndPaswwordRegister(authenticator: authenticator)
            emailAndPasswordRegister.register(withUser: user)
    }
    
}
