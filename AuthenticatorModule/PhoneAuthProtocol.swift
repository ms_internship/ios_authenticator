//
//  PhoneAuthProtocol.swift
//  AuthenticatorModule
//
//  Created by LinuxPlus on 9/18/17.
//  Copyright © 2017 Allam. All rights reserved.
//

import Foundation

protocol PhoneAuthProtocol {

    func authByPhoneNumber(verificationID: String, verificationCode: String, completion: @escaping (String?, AuthError?) -> Void)
}
