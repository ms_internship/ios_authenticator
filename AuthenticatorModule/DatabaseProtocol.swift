//
//  DatabaseProtocol.swift
//  AuthenticatorModule
//
//  Created by LinuxPlus on 9/20/17.
//  Copyright © 2017 Allam. All rights reserved.
//

import Foundation

protocol DatabaseProtocol {
    func saveUser(user : User,_ completion:@escaping (() -> ()))
    func getUser(uid: String, completion: @escaping (User?) ->())
}
