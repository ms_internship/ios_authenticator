//
//  EmailAndPaswwordRegister.swift
//  AuthenticatorModule
//
//  Created by LinuxPlus on 9/10/17.
//  Copyright © 2017 Allam. All rights reserved.
//

import Foundation
import UIKit
import FirebaseAuth

class EmailAndPaswwordRegister: BaseRegisterProtocol{
    var authenticator: RegisterAuthProtocol?
    let authManagar = AuthenticatorManager.shared
    
    
    init(authenticator: RegisterAuthProtocol) {
        self.authenticator = authenticator
        authManagar.dataProvider = EmailAndPasswordDataProvider()
    }
    
    
    func register(withUser user: User){
        self.authenticator?.createUserByEmailAndPassword(email:user.email!, password:user.password!){ (uid, error) in
            if let error = error{
                self.authManagar.AuthFailed(withError: error)
                return
            }
            
            if let uid = uid{
                user.uid = uid
                self.authManagar.registerSuccessfull(withUser: user)
            }
        }
    }
    
}
