//
//  PhoneVerificationController.swift
//  AuthenticatorModule
//
//  Created by LinuxPlus on 9/18/17.
//  Copyright © 2017 Allam. All rights reserved.
//

import Foundation
import UIKit

class PhoneVerificationController {
    
    var authenticator: PhoneAuthProtocol = FirebasePhoneAuthenticator()
    var phoneNumberVerification: BasePhoneVerificationProtocol!
    
    init() {
    }
    
    func verifyPhoneNumber(viewController: UIViewController, phoneNumber: String) {
        phoneNumberVerification = PhoneNumberVerification(viewController: viewController, phoneNumber: phoneNumber, authenticator: authenticator)
        phoneNumberVerification.verify()
    }
    
}
