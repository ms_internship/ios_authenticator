//
//  LinkerDelegate.swift
//  AuthenticatorModule
//
//  Created by LinuxPlus on 9/18/17.
//  Copyright © 2017 Allam. All rights reserved.
//

import Foundation

protocol LinkerDelegate {
    func linkSuccessfull(withMessage message: String)
    func linkFailed(withError error: String)
}
