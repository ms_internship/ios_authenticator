//
//  LoginController.swift
//  AuthenticatorModule
//
//  Created by LinuxPlus on 9/10/17.
//  Copyright © 2017 Allam. All rights reserved.
//

import Foundation
import UIKit
 


class LoginController{
    
    var loginView: UIViewController!
    var authenticator: LoginAuthProtocol!
    var googleLogin: BaseLoginProtocol!
    var emailPasswordLogin: BaseLoginProtocol!
    var facebookAuth: BaseLoginProtocol!
    var emailPasswordRegister: BaseRegisterProtocol!
    var loginViewController: BaseLoginViewController!
    
    
    init(loginViewController: BaseLoginViewController) {
        self.authenticator = FirebaseLoginAuthenticator()
        self.loginViewController = loginViewController
    }
    
    
    func googleLogin(viewController: UIViewController, clientID: String){
        googleLogin = GoogleLogin(viewController: viewController, clientID: clientID, authenticator: authenticator)
        googleLogin.login()
    }
    
    func emailAndPasswordLogin(email:String, password: String){
        emailPasswordLogin = EmailAndPasswordLogin(email: email, password: password, authenticator: authenticator)
        emailPasswordLogin.login()
    }
    
    func facebookLogin(view: UIViewController) {
        facebookAuth = FacebookLogin(authenticator: authenticator)
        facebookAuth.login()
    }
    
}
