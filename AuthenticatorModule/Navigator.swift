//
//  Navigator.swift
//  AuthenticatorModule
//
//  Created by LinuxPlus on 10/3/17.
//  Copyright © 2017 Allam. All rights reserved.
//

import Foundation

class Navigator{
    var loginViewController: BaseLoginViewController?
    var registerViewController: BaseRegisterViewController?
    
    init(from loginVC : BaseLoginViewController?, to registerVC: BaseRegisterViewController?) {
        self.loginViewController = loginVC
        self.registerViewController = registerVC
    }
    
    func navigate(withUser user: User){
        if let loginViewController = self.loginViewController, let registerViewController = self.registerViewController{
            registerViewController.openedFromCompleteProfile = true
            registerViewController.user = user
            loginViewController.navigationController?.pushViewController(registerViewController, animated: true)
        }
    }
}
