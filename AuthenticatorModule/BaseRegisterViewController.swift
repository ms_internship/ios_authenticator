//
//  BaseRegisterViewController.swift
//  AuthenticatorModule
//
//  Created by LinuxPlus on 9/14/17.
//  Copyright © 2017 Allam. All rights reserved.
//


import UIKit
 

class BaseRegisterViewController: UIViewController {
    
    @IBOutlet weak var profilePic: UIButton?
    @IBOutlet weak var firstNameTextField: UITextField?
    @IBOutlet weak var lastNameTextField: UITextField?
    @IBOutlet weak var passwordTextField: UITextField?
    @IBOutlet weak var confirmPaswordTextField: UITextField?
    @IBOutlet weak var emailTextField: UITextField?
    @IBOutlet weak var mainView: UIView?
    @IBOutlet weak var registerButton: UIButton?
    @IBOutlet weak var cancelButton: UIButton?
    @IBOutlet weak var phoneTextField: UITextField?
    @IBOutlet weak var genderTextField: UITextField?
    @IBOutlet weak var birthdayTextField: UITextField?
    @IBOutlet weak var validationErrorTextField: UILabel?
    
    var delegate: AuthenticatorDelegate?
    let imagePicker = UIImagePickerController()
    var registerController: RegisterController!
    var user: User?
    var openedFromCompleteProfile: Bool = false
    var database: DatabaseProtocol!
    let genderTypes = ["male", "female"]
    var validator : ValidationProtocol!
    var settings: RegisterViewSettings = RegisterViewSettings()
    var datePickerView: UIDatePicker = UIDatePicker()
    var genderPickerView: UIPickerView = UIPickerView()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        imagePicker.delegate = self
        mainView?.layer.masksToBounds = true
        mainView?.layer.cornerRadius = 20.0
        registerButton?.layer.cornerRadius = 10.0
        cancelButton?.layer.cornerRadius = 10.0
        circleView(view: profilePic)
        
        genderPickerView.dataSource = self
        genderPickerView.delegate = self
        genderTextField?.inputView = genderPickerView
        
        datePickerView.datePickerMode = UIDatePickerMode.date
        birthdayTextField?.inputView = datePickerView
        datePickerView.addTarget(self, action: #selector(changeBirthdayFromPicker) , for: UIControlEvents.valueChanged)
        genderTextField?.addTarget(self, action: #selector(setDefaultGender), for: UIControlEvents.editingDidBegin)
        
        registerController = RegisterController()
        database = FirebaseDatabase()
        applyRegisterViewSettings(settings: settings)
        
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        completeUserData(user: user)
    }
    
    
    @IBAction func changeProfileImage(_ sender: UIButton) {
        imagePicker.allowsEditing = true
        imagePicker.sourceType = .photoLibrary
        present(imagePicker, animated: true, completion: nil)
    }
    
    
    @IBAction func cancel(_ sender: Any) {
        navigationController?.popViewController(animated: true)
        //        dismiss(animated: true, completion: nil)
    }
    
    @IBAction func registerComplete(_ sender: Any) {
        clearAllErrors()
        guard isRequiredTextFieldsValid() else {return }
        
        // if register is used as complete profile
        if openedFromCompleteProfile && user != nil {
            ActivitySpinner.show("please wait...")
            let userData = getUserFromTextFields(currentUser: self.user)
            database.saveUser(user: userData){
                ActivitySpinner.hide()
                self.delegate?.AuthSuccessfull(withUser: userData)
            }
            return
        }
        
        // check if custom view user must assign email and password outlets
        guard emailTextField != nil && passwordTextField != nil else {
            return
        }
        
        // email and password validation
        if let error = validator.emailValidation(emailTF: emailTextField!){
            self.delegate?.AuthFailed(withError: AuthError(error: error))
            validator.setError(to: emailTextField!)
            displayError(error: error)
            return
        }
        
        if let error = validator.passwordValidation(passwordTF: passwordTextField!){
            self.delegate?.AuthFailed(withError: AuthError(error: error))
            validator.setError(to: passwordTextField!)
            displayError(error: error)
            return
        }
        
        if  passwordTextField?.text! != confirmPaswordTextField?.text!{
            let error = "Confirm password doesn't match"
            self.delegate?.AuthFailed(withError: AuthError(error: error))
            validator.setError(to: passwordTextField!)
            validator.setError(to: confirmPaswordTextField!)
            displayError(error: error)
            return
        }
        
        
        
        ActivitySpinner.show("please wait...")
        let registeredUser = getUserFromTextFields()
        registerController.emailAndPasswordRegister(withUser: registeredUser)
        
        // if user choose to merge phone with register link in one account
//        if settings.canLoginWithPhoneNumber, let phoneTF = phoneTextField{
//            registerController.linkWithPhone(viewController: self, phoneNumber: phoneTF.text!)
//        }
        
    }

    
//    func registerSuccessfull(authType: AuthType, uid: String){
//        let userData = getDataFromTextFields(currentUser: self.user)
//        userData.uid = uid
//        database.saveUser(user: userData) { 
//            ActivitySpinner.hide()
//        }
//        
//        delegate?.AuthRegisterSuccsessfull(withMessage: "register successfull")
//        ActivitySpinner.hide()
//    }
//    
//    
//    func registerFailed(withError error:String){
//        ActivitySpinner.hide()
//        delegate?.AuthRegisterFailed(withError: error)
//    }
    
}

// profile picture picker
extension BaseRegisterViewController: UIImagePickerControllerDelegate, UINavigationControllerDelegate{
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        
        if let pickedImage = info[UIImagePickerControllerEditedImage] as? UIImage {
            profilePic?.setImage(pickedImage, for: .normal)
        } else {
            profilePic?.setImage(#imageLiteral(resourceName: "anonymous_user_profile"), for: .normal)
        }
        
        dismiss(animated: true, completion: nil)
    }
    
}

// birthday and gender pickers
extension BaseRegisterViewController: UIPickerViewDelegate, UIPickerViewDataSource{
    
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return genderTypes.count
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return genderTypes[row]
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        genderTextField?.text = genderTypes[row]
    }
    
}

// private helper functions to organize code
extension BaseRegisterViewController{
    // complete textfields of register view according to the user coming from login providers
    fileprivate func completeUserData(user: User?){
        if let user = user{
            firstNameTextField?.text = user.firstName
            lastNameTextField?.text = user.lastName
            emailTextField?.text = user.email
            phoneTextField?.text = user.phone
            genderTextField?.text = user.gender
            birthdayTextField?.text = user.birthday
            passwordTextField?.isHidden = true
            confirmPaswordTextField?.isHidden = true
            registerButton?.setTitle("complete", for: UIControlState.normal)
        }
    }
    
    
    // circle image picker button
    fileprivate func circleView(view: UIView?){
        if let view = view{
            view.layer.cornerRadius = view.frame.size.width / 2;
            view.clipsToBounds = true;
            view.layer.borderWidth = 2
            view.layer.borderColor = UIColor.white.cgColor
        }
    }
    
    @objc fileprivate func changeBirthdayFromPicker(){
        let dateFormater = DateFormatter()
        dateFormater.dateFormat = "dd-MMM-YYYY"
        let birthday = dateFormater.string(from: datePickerView.date)
        birthdayTextField?.text = birthday
    }
    
    @objc fileprivate func setDefaultGender(){
        genderTextField?.text = genderTypes[0]
    }
    
    
    fileprivate func applyRegisterViewSettings(settings: RegisterViewSettings){
        profilePic?.isHidden = !settings.isProfilePicEnabled
        firstNameTextField?.isHidden = !settings.isFirstNameEnabled
        lastNameTextField?.isHidden = !settings.isLastNameEnabled
        emailTextField?.isHidden = !settings.isEmailEnabled
        phoneTextField?.isHidden = !settings.isPhoneEnabled
        passwordTextField?.isHidden = !settings.isPhoneEnabled
        confirmPaswordTextField?.isHidden = !settings.isPasswordEnabled
        genderTextField?.isHidden = !settings.isGenderEnabled
        birthdayTextField?.isHidden = !settings.isBirthdayEnabled
    }
    
    fileprivate func isRequiredTextFieldsValid() -> Bool{
        if settings.requiredTextFields.isFirstNameRequired && isEmptyTextField(firstNameTextField!){
            return false
        }
        
        if settings.requiredTextFields.isLastNameRequired && isEmptyTextField(lastNameTextField!){
            return false
        }
        
        if settings.requiredTextFields.isEmailRequired && isEmptyTextField(emailTextField!){
            return false
        }
        
        if settings.requiredTextFields.isPhoneRequired && isEmptyTextField(phoneTextField!){
            return false
        }
        
        if settings.requiredTextFields.isGenderRequired && isEmptyTextField(genderTextField!){
            return false
        }
        
        if settings.requiredTextFields.isBirthdayRequired && isEmptyTextField(birthdayTextField!){
            return false
        }
        
        return true
    }
    
    fileprivate func isEmptyTextField(_ textField: UITextField) -> Bool{
        if textField.text == ""{
            validator.setError(to: textField)
            displayError(error: "\(textField.placeholder ?? "this field ") is required")
            return true
        }
        return false
    }
    
    //collect data to save it to database
    fileprivate func getUserFromTextFields(currentUser: User? = nil) -> User{
        let user = User()
        if currentUser != nil{
            user.uid = currentUser?.uid
        }else{
            user.password = passwordTextField?.text
        }
        
        user.firstName = firstNameTextField?.text
        user.lastName = lastNameTextField?.text
        user.email = emailTextField?.text
        user.phone = phoneTextField?.text
        user.gender = genderTextField?.text
        user.birthday = birthdayTextField?.text
        return user
    }
    
    fileprivate func clearAllErrors(){
        firstNameTextField?.isHidden = !settings.isFirstNameEnabled
        lastNameTextField?.layer.borderWidth = 0
        emailTextField?.layer.borderWidth = 0
        phoneTextField?.layer.borderWidth = 0
        passwordTextField?.layer.borderWidth = 0
        confirmPaswordTextField?.layer.borderWidth = 0
        genderTextField?.layer.borderWidth = 0
        birthdayTextField?.layer.borderWidth = 0
    }
    
    fileprivate func displayError(error: String){
        validationErrorTextField?.text = error
        self.validationErrorTextField?.alpha = 1
        UIView.animate(withDuration: 4) {
            self.validationErrorTextField?.alpha = 0.0
        }
    }
}
