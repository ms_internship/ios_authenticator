//
//  FacebookLogin.swift
//  AuthenticatorModule
//
//  Created by LinuxPlus on 9/9/17.
//  Copyright © 2017 Allam. All rights reserved.
//

import Foundation
import UIKit
import FacebookCore
import FacebookLogin


class FacebookLogin:NSObject, BaseLoginProtocol {
    var authenticator: LoginAuthProtocol?
    let authManager = AuthenticatorManager.shared
    
    init( authenticator: LoginAuthProtocol) {
        super.init()
        self.authenticator = authenticator
        authManager.dataProvider = FacebookDataProvider()
    }
    
    func login() {
        let loginManager = LoginManager()
        loginManager.logIn([.publicProfile , .email, .userFriends]) { (loginResult) in
            switch loginResult{
            case .success( _, _, let token):
                ActivitySpinner.show("Facebook Authentication \nplease wait...")
                self.authenticator?.authByFacebook(accessToken: token.authenticationToken, completion: { (uid, error) in
                    guard error == nil else{
                        self.authManager.AuthFailed(withError: error!)
                        return
                    }
                    if let uid = uid{
                        self.authManager.AuthSuccessfull(uid: uid)
                    }
                })
            case .failed(let error):
                self.authManager.AuthFailed(withError: AuthError(error: error.localizedDescription))
            case .cancelled:
                self.authManager.AuthFailed(withError: AuthError(error: "The user canceled the sign-in flow"))
            }
        }
    }
    
    
    
}
