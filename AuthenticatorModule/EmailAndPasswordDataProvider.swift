//
//  EmailAndPasswordDataProvider.swift
//  AuthenticatorModule
//
//  Created by LinuxPlus on 10/4/17.
//  Copyright © 2017 Allam. All rights reserved.
//

import Foundation
import FirebaseAuth


class EmailAndPasswordDataProvider: DataProviderProtocol {
    
    func getUserData(completion: @escaping (User?, AuthError?) -> ()) {
        if let currentUser = Auth.auth().currentUser{
            let user = User(withEmail: currentUser.email)
            completion(user, nil)
        }else{
            let authError = AuthError(error: "couldn't fetch user's email")
            completion(nil, authError)
        }
    }
}
