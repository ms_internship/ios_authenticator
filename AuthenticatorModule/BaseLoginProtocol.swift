//
//  BaseLoginProtocol.swift
//  AuthenticatorModule
//
//  Created by LinuxPlus on 9/7/17.
//  Copyright © 2017 Allam. All rights reserved.
//

import Foundation
import UIKit

protocol BaseLoginProtocol {
    var authenticator: LoginAuthProtocol? {get set}
    func login()
}



