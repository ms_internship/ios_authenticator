//
//  AuthenticatorDelegate.swift
//  AuthenticatorModule
//
//  Created by LinuxPlus on 9/10/17.
//  Copyright © 2017 Allam. All rights reserved.
//

import Foundation

protocol AuthenticatorDelegate {
    func AuthSuccessfull(withUser user: User)
    func AuthFailed(withError error: AuthError)
    func AuthLoginSuccsessfull(withMessage message: String)
    func AuthLoginFailed(withError error: String)
    func AuthRegisterSuccsessfull(withMessage message: String)
    func AuthRegisterFailed(withError error: String)
    
    func getRegisterViewController()-> BaseRegisterViewController?
    func getLoginViewController()-> BaseLoginViewController?
}

extension AuthenticatorDelegate{
    func AuthSuccessfull(withUser user: User){}
    func AuthFailed(withError error: AuthError){}
    
    func AuthLoginSuccsessfull(withMessage message: String){}
    func AuthLoginFailed(withError error: String){}
    func AuthRegisterSuccsessfull(withMessage message: String){}
    func AuthRegisterFailed(withError error: String){}
    
    func getRegisterViewController()-> BaseRegisterViewController?{return nil}
    func getLoginViewController()-> BaseLoginViewController?{return nil}
}
