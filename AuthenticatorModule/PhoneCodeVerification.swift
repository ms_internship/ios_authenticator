//
//  PohneCodeVerification.swift
//  AuthenticatorModule
//
//  Created by LinuxPlus on 10/6/17.
//  Copyright © 2017 Allam. All rights reserved.
//

import Foundation
import UIKit

class PhoneCodeVerification {

    func verifySentCode(viewController: UIViewController,resendBlock: @escaping () -> Void ,completion: @escaping (String) -> Void){
        let alertController = UIAlertController(title: "Code verification", message: "Enter the verification code from your mobile", preferredStyle: .alert)
        
        alertController.addTextField(configurationHandler: { (textField) -> Void in
            textField.placeholder = "Verification Code"
            textField.textAlignment = .center
        })
        
        let doneAction = UIAlertAction(title: "Done", style: .default) { (alertAction) in
            if let verificationCode = alertController.textFields![0].text{
                completion(verificationCode)
            }
        }
        let sendAgainAction = UIAlertAction(title: "Send code again", style: .default) { (alertAction) in
            resendBlock()
        }
        
        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel, handler: nil)
        
        
        alertController.addAction(doneAction)
        alertController.addAction(sendAgainAction)
        alertController.addAction(cancelAction)
        
        viewController.present(alertController, animated: true, completion: nil)
    }

}
