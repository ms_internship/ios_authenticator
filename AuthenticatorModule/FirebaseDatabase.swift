//
//  FirebaseDatabase.swift
//  AuthenticatorModule
//
//  Created by LinuxPlus on 9/20/17.
//  Copyright © 2017 Allam. All rights reserved.
//

import Foundation
import FirebaseDatabase

class FirebaseDatabase: DatabaseProtocol {
    var databaseRef : DatabaseReference!
    
    
    init() {
        databaseRef = Database.database().reference().child("users")
    }
    
    
    func saveUser(user : User, _ completion:@escaping (() -> ())){
        let userDict = user.getDictionary()
        databaseRef.child(user.uid!).setValue(userDict)
        completion()
    }
    
    
    func getUser(uid: String, completion: @escaping (User?) ->() ){
        databaseRef.child(uid).observeSingleEvent(of: .value, with: { (snapshot) in
            if let userDict = snapshot.value as? [String: Any]{
                let user = User(withDictionary: userDict)
               completion(user)
            }else{
               completion(nil)
            }
        })
    }
    
    
}
