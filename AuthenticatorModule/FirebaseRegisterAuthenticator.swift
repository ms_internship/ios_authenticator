//
//  FirebaseRegisterAuthenticator.swift
//  AuthenticatorModule
//
//  Created by LinuxPlus on 9/18/17.
//  Copyright © 2017 Allam. All rights reserved.
//

import Foundation
import FirebaseAuth

class FirebaseRegisterAuthenticator: RegisterAuthProtocol {
    
    let auth = Auth.auth()
    
    func createUserByEmailAndPassword(email: String, password: String, completion: @escaping (String?, AuthError?) -> Void) {
        
        auth.createUser(withEmail:email , password: password) { (user, error) in
            if let error = error{
                completion(nil, AuthError(error: error.localizedDescription))
                return
            }
            if let user = user{
                completion(user.uid, nil)
            }
        }
    }
    
    
}
