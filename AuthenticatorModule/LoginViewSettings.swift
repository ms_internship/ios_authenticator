//
//  LoginViewSettings.swift
//  AuthenticatorModule
//
//  Created by LinuxPlus on 9/25/17.
//  Copyright © 2017 Allam. All rights reserved.
//

import Foundation

class LoginViewSettings {
    var isFacebookLoginEnabled: Bool = true
    var isGoogleLoginEnabled: Bool = true
    var isEmailAndPasswordEnabled: Bool = true
    var isPhoneNumberEnabled: Bool = true
    var isCreateUserEnabled: Bool = true
}
