//
//  Authenticator.swift
//  AuthenticatorModule
//
//  Created by LinuxPlus on 9/20/17.
//  Copyright © 2017 Allam. All rights reserved.
//

import Foundation
import UIKit
class Authenticator:AuthenticatorDelegate {

    var loginViewController: BaseLoginViewController?
    var registerViewController: BaseRegisterViewController?
    var delegate: AuthenticatorDelegate?
    let authManager = AuthenticatorManager.shared
    
    init(builder: AuthenticatorBuilder) {
        // get login and register views if user set them in builder
        self.loginViewController = builder.loginViewController
        self.registerViewController = builder.registerViewController
        
        // set googleID if user need google Login
        if let googleID = builder.googleClientID{
            self.loginViewController?.clientId = googleID
        }
        
        // set default validator
        let defaultValidator = builder.validator != nil ? builder.validator : Validator()
        self.loginViewController?.validator = defaultValidator
        self.registerViewController?.validator = defaultValidator
        
        // set delegates
        self.loginViewController?.delegate = self
        self.registerViewController?.delegate = self
        
        // set navigator if user enabled complete profile
        if builder.isCompleteProfileEnabled{
            let completeProfileNavigator = Navigator(from: self.loginViewController, to: self.registerViewController)
            authManager.delegate = self
           authManager.navigator = completeProfileNavigator
        }
        
        // if setting is link with phone set it to authManager
        if let linkWithPhone = registerViewController?.settings.canLoginWithPhoneNumber{
           authManager.canLoginWithPhone = linkWithPhone
            authManager.registerViewController = self.registerViewController
        }
    }
    
    
    func getLoginViewController() -> BaseLoginViewController?{
       return self.loginViewController
    }
    
    func getRegisterViewController() -> BaseRegisterViewController?{
       return self.registerViewController
    }
     //-----
    func AuthSuccessfull(withUser user: User) {
        delegate?.AuthSuccessfull(withUser: user)
    }
    
    func AuthFailed(withError error: AuthError) {
        delegate?.AuthFailed(withError: error)
    }
    //------
    func AuthLoginSuccsessfull(withMessage message: String) {
        delegate?.AuthLoginSuccsessfull(withMessage: message)
    }
    
    func AuthLoginFailed(withError error: String) {
        delegate?.AuthLoginFailed(withError: error)
    }
    
    
    func AuthRegisterSuccsessfull(withMessage message: String) {
        delegate?.AuthRegisterSuccsessfull(withMessage: message)
    }
    func AuthRegisterFailed(withError error: String) {
        delegate?.AuthRegisterFailed(withError: error)
    }
}
