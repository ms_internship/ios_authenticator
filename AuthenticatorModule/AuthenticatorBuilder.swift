//
//  AuthenticatorBuilder.swift
//  AuthenticatorModule
//
//  Created by LinuxPlus on 9/20/17.
//  Copyright © 2017 Allam. All rights reserved.
//

import Foundation
import UIKit

class AuthenticatorBuilder {
    var loginViewController: BaseLoginViewController?
    var registerViewController: BaseRegisterViewController?
    var googleClientID: String?
    var validator: ValidationProtocol?
    var isCompleteProfileEnabled: Bool = false
    
    
    func addLoginView(customLoginView: BaseLoginViewController? = nil, withSettings settings: LoginViewSettings? = nil) -> AuthenticatorBuilder{
        if let loginView = customLoginView{
            self.loginViewController = loginView
        }else{
            self.loginViewController = getDefaultView(identifier: "BaseLoginViewController") as? BaseLoginViewController
        }
        if let settings = settings{
                self.loginViewController?.settings = settings
        }
        return self
    }
    
    func addRegisterView(customRegisterView: BaseRegisterViewController? = nil, withSettings settings: RegisterViewSettings? = nil) -> AuthenticatorBuilder {
        if let registerView = customRegisterView{
            self.registerViewController = registerView
        }else{
            self.registerViewController = getDefaultView(identifier: "defaultRegisterViewController") as? BaseRegisterViewController
        }
        if let settings = settings{
            self.registerViewController?.settings = settings
        }
        return self
    }
    
    func setCustomValidator(_ validator: Validator) -> AuthenticatorBuilder{
        self.validator = validator
        return self
    }
    
    func setGoogleClientID(clientID: String) -> AuthenticatorBuilder {
        googleClientID = clientID
        return self
    }
    
    func enableCompleteProfile() -> AuthenticatorBuilder{
        self.isCompleteProfileEnabled = true
        return self
    }
    
    
    func build() -> Authenticator{
        return Authenticator(builder: self)
    }
    
    private func getDefaultView(identifier: String) -> UIViewController?{
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        return storyboard.instantiateViewController(withIdentifier:identifier)
    }
    
}
