//
//  UserViewController.swift
//  AuthenticatorModule
//
//  Created by LinuxPlus on 9/20/17.
//  Copyright © 2017 Allam. All rights reserved.
//

import UIKit

class UserViewController: UIViewController, AuthenticatorDelegate {
    
    let clientId = "429790652190-cba95f2o4soshspo20h7mk69dh9o0bn4.apps.googleusercontent.com"
    var authenticator: Authenticator!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let registerSettings = RegisterViewSettings()
        registerSettings.requiredTextFields.isEmailRequired = true
        registerSettings.requiredTextFields.isPhoneRequired = true
        
        let customLogin = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "customLoginViewController") as! BaseLoginViewController
        
        authenticator = AuthenticatorBuilder()
            .addLoginView(customLoginView: customLogin)
            .addRegisterView(withSettings: registerSettings)
            .setGoogleClientID(clientID: clientId)
            .enableCompleteProfile()
            .build()
        authenticator.delegate = self
    }
    
    override func viewWillAppear(_ animated: Bool) {
        navigationController?.navigationBar.isHidden = true
    }
    
    @IBAction func loginTapped(_ sender: Any) {
        navigationController?.pushViewController(authenticator.getLoginViewController()!, animated: true)
    }
    
    @IBAction func registerTapped(_ sender: Any) {
       navigationController?.pushViewController(authenticator.getRegisterViewController()!, animated: true)
    }
    
    
    
    
    func AuthSuccessfull(withUser user: User) {
        print(user)
        displayAlert(message: user.email!)
    }
    
    func AuthFailed(withError error: AuthError) {
         displayAlert(message: error.localizedDescription)
    }
    
    
    
    
    
    func AuthLoginSuccsessfull(withMessage message: String) {
        displayAlert(message: message)
    }
    
    func AuthRegisterSuccsessfull(withMessage message: String) {
        displayAlert(message: message)
        
    }
    
    func AuthLoginFailed(withError error: String) {
        displayAlert(message: error)
        
    }
    
    func AuthRegisterFailed(withError error: String) {
        displayAlert(message: error)
        
    }
    
    func displayAlert(message: String) {
        let alert = UIAlertController(title: "Authenticator", message: message, preferredStyle: UIAlertControllerStyle.alert)
        let action = UIAlertAction(title: "OK", style: .default, handler: nil)
        
        alert.addAction(action)
        present(alert, animated: true, completion: nil)
        
    }
    
}
