//
//  RegisterViewSettings.swift
//  AuthenticatorModule
//
//  Created by LinuxPlus on 9/25/17.
//  Copyright © 2017 Allam. All rights reserved.
//

import Foundation

class RegisterViewSettings {
    
    var requiredTextFields = RequiredTextFields()
    
    var isProfilePicEnabled: Bool = true
    var isFirstNameEnabled: Bool = true
    var isLastNameEnabled: Bool = true
    var isEmailEnabled: Bool = true
    var isPasswordEnabled: Bool = true
    var isPhoneEnabled: Bool = true
    var isGenderEnabled: Bool = true
    var isBirthdayEnabled: Bool = true
    
    var canLoginWithPhoneNumber:Bool = false
}

struct RequiredTextFields {
    var isFirstNameRequired: Bool = false
    var isLastNameRequired: Bool = false
    var isEmailRequired: Bool = false
    var isPhoneRequired: Bool = false
    var isGenderRequired: Bool = false
    var isBirthdayRequired: Bool = false
}
