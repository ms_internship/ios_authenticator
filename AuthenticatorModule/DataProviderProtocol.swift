//
//  DataProviderProtocol.swift
//  AuthenticatorModule
//
//  Created by LinuxPlus on 10/2/17.
//  Copyright © 2017 Allam. All rights reserved.
//

import Foundation

protocol DataProviderProtocol {
    func getUserData(completion:@escaping (User?, AuthError?) -> ())
}
