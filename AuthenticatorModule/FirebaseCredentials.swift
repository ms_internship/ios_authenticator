//
//  FirebaseCredentials.swift
//  AuthenticatorModule
//
//  Created by LinuxPlus on 9/20/17.
//  Copyright © 2017 Allam. All rights reserved.
//

import Foundation
import UIKit
import FirebaseAuth

class FirebaseCredentials {
    
    static func getEmailAndPasswrodCredential(emailTF: UITextField, passwordTF: UITextField) -> AuthCredential {
        let credential = EmailAuthProvider.credential(withEmail: emailTF.text!, password: passwordTF.text!)
        return credential
    }

}
