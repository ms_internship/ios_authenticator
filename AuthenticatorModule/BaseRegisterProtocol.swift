//
//  BaseRegisterProtocol.swift
//  AuthenticatorModule
//
//  Created by LinuxPlus on 9/7/17.
//  Copyright © 2017 Allam. All rights reserved.
//

import Foundation
import FirebaseAuth

protocol BaseRegisterProtocol {
    var authenticator: RegisterAuthProtocol? {get set}
    
    func register(withUser user: User)
}
