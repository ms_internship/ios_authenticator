//
//  PhoneDataProvider.swift
//  AuthenticatorModule
//
//  Created by LinuxPlus on 10/2/17.
//  Copyright © 2017 Allam. All rights reserved.
//

import Foundation
import FirebaseAuth


class PhoneDataProvider: DataProviderProtocol {
    
    func getUserData(completion: @escaping (User?, AuthError?) -> ()) {
        if let currentUser = Auth.auth().currentUser{
            let user = User(withPhone: currentUser.phoneNumber)
            completion(user, nil)
        }else{
            let authError = AuthError(error: "couldn't fetch phone number")
            completion(nil, authError)
        }
    }
}
