//
//  GoogleLogin.swift
//  AuthenticatorModule
//
//  Created by LinuxPlus on 9/7/17.
//  Copyright © 2017 Allam. All rights reserved.
//

import Foundation
import GoogleSignIn


class GoogleLogin:NSObject, BaseLoginProtocol, GIDSignInDelegate, GIDSignInUIDelegate{
    var authenticator: LoginAuthProtocol?
    let authManager = AuthenticatorManager.shared
    
    var googleView: UIViewController!
    
    init(viewController: UIViewController, clientID: String, authenticator: LoginAuthProtocol) {
        super.init()
        self.authenticator = authenticator
        googleView = viewController
        initializeGISignIn(clientID: clientID)
        
        self.authManager.dataProvider = GoogleDataProvider()
    }
    
    func login() {
        GIDSignIn.sharedInstance().signOut()
        GIDSignIn.sharedInstance().signIn()
    }
    
    func sign(_ signIn: GIDSignIn!, didSignInFor user: GIDGoogleUser!, withError error: Error!) {
        guard error == nil else{
            self.authManager.AuthFailed(withError: AuthError(error: error.localizedDescription))
            return
        }
        
        // authenticate by google to the server
        ActivitySpinner.show("Google Authentication \nplease wait...")
        authenticator?.authByGmail(idToken: user.authentication.idToken, accessToken: user.authentication.accessToken, completion: { (uid, error) in
            guard error == nil else{
                self.authManager.AuthFailed(withError: error!)
                return
            }
            if let uid = uid{
                self.authManager.AuthSuccessfull(uid: uid)
            }
        })
    }
    
    func sign(inWillDispatch signIn: GIDSignIn!, error: Error!) {
        
    }
    
    func sign(_ signIn: GIDSignIn!, present viewController: UIViewController!) {
        googleView.present(viewController, animated: true, completion: nil)
    }
    
    func sign(_ signIn: GIDSignIn!, dismiss viewController: UIViewController!) {
        googleView.dismiss(animated: true, completion: nil)
        ActivitySpinner.show("Google login \nplease wait...")
    }
    
    private func initializeGISignIn(clientID:String){
        GIDSignIn.sharedInstance().clientID = clientID
        GIDSignIn.sharedInstance().uiDelegate = self
        GIDSignIn.sharedInstance().delegate = self
    }
    
}
