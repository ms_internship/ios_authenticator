//
//  FirebaseLoginAuthenticator.swift
//  AuthenticatorModule
//
//  Created by LinuxPlus on 9/9/17.
//  Copyright © 2017 Allam. All rights reserved.
//

import Foundation
import FirebaseAuth
 

class FirebaseLoginAuthenticator: LoginAuthProtocol{
    
    var delegate: AuthenticatorDelegate?
    var auth: Auth
    var authManager: AuthenticatorManager!
    init() {
        auth = Auth.auth()
        authManager = AuthenticatorManager.shared
    }
    
    func authByGmail(idToken: String ,accessToken: String, completion:@escaping (String?, AuthError?) -> Void) {
        let googleCredintal = GoogleAuthProvider.credential(withIDToken: idToken, accessToken: accessToken)
        auth.signIn(with: googleCredintal) { (user, error) in
            if let error = error{
                completion(nil, AuthError(error: error.localizedDescription))
                return
            }
            if let uid = user?.uid{
//                let dataProvider = GoogleDataProvider()
//                self.authManager.dataProvider = dataProvider
//                self.authManager.AuthSuccessfull(uid: uid)
                completion(uid, nil)
            }else{
               completion(nil, AuthError(error: "no uid found"))
            }
        }
    }
    
    func authByEmailAndPassword(email: String, password: String, completion: @escaping (String?, AuthError?) -> Void) {
        auth.signIn(withEmail: email, password: password) { (user, error) in
            if let error = error{
                completion(nil, AuthError(error: error.localizedDescription))
                return
            }
            
            if let uid = user?.uid{
                completion(uid, nil)
            }else{
                completion(nil, AuthError(error: "no uid found"))
            }

        }
    }
    
    func authByFacebook(accessToken: String, completion: @escaping (String?, AuthError?) -> Void) {
        let facebookCredintal = FacebookAuthProvider.credential(withAccessToken: accessToken)
        auth.signIn(with: facebookCredintal) { user, error in
            if let error = error{
                self.delegate?.AuthFailed(withError: AuthError(error: error.localizedDescription))
                return
            }
            if let uid = user?.uid{
                 completion(uid, nil)
            }else{
                completion(nil, AuthError(error: "no uid found"))
            }
        }
    }
    
}
