//
//  User.swift
//  AuthenticatorModule
//
//  Created by LinuxPlus on 9/18/17.
//  Copyright © 2017 Allam. All rights reserved.
//

import Foundation
import GoogleSignIn

class User {
    var uid: String?
    var imageUrl: URL?
    var firstName: String?
    var lastName: String?
    var email: String?
    var password: String?
    var phone: String?
    var gender: String?
    var birthday: String?
    
    init() {
        
    }
    
    init(withDictionary dictionary: [String:Any]) {
        self.uid = dictionary["uid"] as? String
        self.imageUrl = dictionary["image_url"] as? URL
        self.firstName = dictionary["first_name"] as? String
        self.lastName = dictionary["last_name"] as? String
        self.email = dictionary["email"] as? String
        self.password = dictionary["password"] as? String
        self.phone = dictionary["phone"] as? String
        self.gender = dictionary["gender"] as? String
        self.birthday = dictionary["birthday"] as? String
    }
    
    init(withFacebook facebookDict: [String:Any]) {
        self.uid = facebookDict["uid"] as? String
        self.imageUrl = facebookDict["image_url"] as? URL
        self.firstName = facebookDict["first_name"] as? String
        self.lastName = facebookDict["last_name"] as? String
        self.email = facebookDict["email"] as? String
        self.password = facebookDict["password"] as? String
        self.phone = facebookDict["phone"] as? String
        self.gender = facebookDict["gender"] as? String
        self.birthday = facebookDict["birthday"] as? String
    }
    
    init(withGoogle googleUser: GIDGoogleUser) {
        self.imageUrl = googleUser.profile.imageURL(withDimension: 120)
        self.firstName = googleUser.profile.givenName
        self.lastName = googleUser.profile.familyName
        self.email = googleUser.profile.email
    }
    
    init(withPhone phone: String?) {
        self.phone = phone
    }
    
    init(withEmail email: String?) {
        self.email = email
    }
    
    func getDictionary() -> [String:Any]{
        var dict: [String: Any] = [:]
        dict["uid"] = self.uid
        dict["image_url"] = self.imageUrl
        dict["first_name"] = self.firstName
        dict["last_name"] = self.lastName
        dict["email"] = self.email
        dict["password"] = self.password
        dict["phone"] = self.phone
        dict["gender"] = self.gender
        dict["birthday"] = self.birthday
        return dict
    }
    
    
}

