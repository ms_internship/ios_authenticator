//
//  BaseLoginViewController.swift
//  AuthenticatorModule
//
//  Created by LinuxPlus on 9/6/17.
//  Copyright © 2017 Allam. All rights reserved.
//

import UIKit
 

class BaseLoginViewController: UIViewController{
    
    var clientId: String?
    
    @IBOutlet weak var emailTextField: UITextField?
    @IBOutlet weak var passwordTextField: UITextField?
    @IBOutlet weak var phoneTextField: UITextField?
    @IBOutlet weak var loginButton: UIButton?
    @IBOutlet weak var createUserButton: UIButton?
    @IBOutlet weak var googleButton: UIButton?
    @IBOutlet weak var facebookButton: UIButton?
    @IBOutlet weak var phoneButton: UIButton?
    
    var loginController:LoginController!
    var phoneController: PhoneVerificationController?
    var settings: LoginViewSettings = LoginViewSettings()
    var delegate: AuthenticatorDelegate?
    var validator: ValidationProtocol!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        loginController = LoginController(loginViewController: self)
        phoneController = PhoneVerificationController()
        applyLoginViewSettings(settings: settings)
    }
    
    
    @IBAction func emailAndPasswordLoginTapped(_ sender: Any) {
        // remove red border error
        emailTextField?.layer.borderWidth = 0
        passwordTextField?.layer.borderWidth = 0
        
        // email validation
        if let error = validator.emailValidation(emailTF: emailTextField!){
            self.delegate?.AuthFailed(withError: AuthError(error: error))
            validator.setError(to: emailTextField!)
            return
        }
        
        //password validation
        if let error = validator.passwordValidation(passwordTF: passwordTextField!){
            self.delegate?.AuthFailed(withError: AuthError(error: error))
            validator.setError(to: passwordTextField!)
            return
        }
        
        if let email = emailTextField, let password = passwordTextField{
            ActivitySpinner.show("please wait...")
            loginController.emailAndPasswordLogin(email: email.text! , password: password.text!)
            email.text = ""
            password.text = ""
        }
    }
    
    
    @IBAction func createNewUserTapped(_ sender: Any) {
        if let registerViewController = delegate?.getRegisterViewController(){
            navigationController?.pushViewController(registerViewController, animated: true)
        }
    }
    
    
    @IBAction func googleLoginTapped(_ sender: Any) {
        if let clientId = clientId{
            loginController.googleLogin(viewController: self, clientID: clientId)
        }
    }
    
    @IBAction func facebookLoginTapped(_ sender: Any) {
        loginController.facebookLogin(view: self)
    }
    
    @IBAction func phoneButtonTapped(_ sender: Any) {
        if let phone = phoneTextField{
            phoneController?.verifyPhoneNumber(viewController: self, phoneNumber: phone.text!)
            phone.text = ""
        }
    }
    
    private func applyLoginViewSettings(settings: LoginViewSettings){
        facebookButton?.isHidden = !settings.isFacebookLoginEnabled
        googleButton?.isHidden = !settings.isGoogleLoginEnabled
        createUserButton?.isHidden = !settings.isCreateUserEnabled
        
        loginButton?.isHidden = !settings.isEmailAndPasswordEnabled
        emailTextField?.isHidden = !settings.isEmailAndPasswordEnabled
        passwordTextField?.isHidden = !settings.isEmailAndPasswordEnabled
        
        phoneButton?.isHidden = !settings.isPhoneNumberEnabled
        phoneTextField?.isHidden = !settings.isPhoneNumberEnabled
    }
}

