//
//  EmailAndPasswordWithPhoneLinker.swift
//  AuthenticatorModule
//
//  Created by LinuxPlus on 10/5/17.
//  Copyright © 2017 Allam. All rights reserved.
//

import Foundation
import UIKit
import FirebaseAuth


class EmailPasswordWithPhoneLinker {
    private var user: User!
    private var phoneVerification: BasePhoneVerificationProtocol!
    private var phoneAuth: PhoneAuthProtocol!
    private let authManager = AuthenticatorManager.shared
    
    init(linkUser user: User, viewController: UIViewController) {
        self.user = user
        phoneAuth = FirebasePhoneAuthenticator()
        phoneVerification = PhoneNumberVerification(viewController: viewController, phoneNumber: user.phone!, authenticator: phoneAuth)
    }
    
    
    func verifyPhone() {
        phoneVerification.verify()
    }
    
    func link() {
        if let currentUser = Auth.auth().currentUser, let credential = getEmailAndPasswordCredential(){
            currentUser.link(with: credential, completion: { (fireUser, error) in
                if let error = error{
                    self.authManager.AuthFailed(withError: AuthError(error: error.localizedDescription))
                }else{
                    self.authManager.linkSuccessfull(withUser: self.user)
                }
            })
        }
    }
    
    private func getEmailAndPasswordCredential() -> AuthCredential? {
        if let email = user.email, let password = user.password{
            return EmailAuthProvider.credential(withEmail: email, password: password)
        }
        return nil
    }
    
}
