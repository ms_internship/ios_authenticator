//
//  AuthenticatorManager.swift
//  AuthenticatorModule
//
//  Created by LinuxPlus on 10/2/17.
//  Copyright © 2017 Allam. All rights reserved.
//

import Foundation
import UIKit



class AuthenticatorManager {
    var delegate: AuthenticatorDelegate?
    var dataProvider: DataProviderProtocol?
    var registerViewController: BaseRegisterViewController?
    var navigator: Navigator?
    var canLoginWithPhone: Bool = false
    private var database: DatabaseProtocol?
    private var linker: EmailPasswordWithPhoneLinker!
    private var linkerFlag = false // to indicate if it coming from link or not
    
    static let shared: AuthenticatorManager	= {
        return AuthenticatorManager()
    }()
    
    private init(){
    database = FirebaseDatabase()
    }
    
    func AuthSuccessfull(uid: String){
        ActivitySpinner.hide()
        database?.getUser(uid: uid) { (existUser) in
            if let existUser = existUser {
                self.delegate?.AuthSuccessfull(withUser: existUser)
            }else{
                self.dataProvider?.getUserData(completion: { (newUser, error) in
                    if let user = newUser{
                        user.uid = uid
                        self.navigator?.navigate(withUser: user)
                    }
                })
            }
        }
    }
    
    func registerSuccessfull(withUser user: User){
        ActivitySpinner.hide()
        if self.canLoginWithPhone{ // link withphone
            self.linker = EmailPasswordWithPhoneLinker(linkUser: user, viewController: registerViewController!)
            self.linker.verifyPhone()
            self.linkerFlag = true
        }else{ // save user
            database?.saveUser(user: user){
                self.delegate?.AuthSuccessfull(withUser: user)
            }
        }
        
        
        
    }
    
    func PhoneVerificationSuccessfull(uid: String){
        ActivitySpinner.hide()
        if linkerFlag{
            self.linker.link()
            return
        }
        
        database?.getUser(uid: uid) { (existUser) in
            if let existUser = existUser {
                self.delegate?.AuthSuccessfull(withUser: existUser)
            }else{
                self.dataProvider?.getUserData(completion: { (newUser, error) in
                    if let user = newUser{
                        user.uid = uid
                        self.navigator?.navigate(withUser: user)
                    }
                })
            }
        }
    }
    
    
    func linkSuccessfull(withUser user: User){
        database?.saveUser(user: user) {
            self.delegate?.AuthSuccessfull(withUser: user)
        }
    }
    
    func linkFailed(withError error: AuthError){
        delegate?.AuthFailed(withError: error)
    }
    
    func AuthFailed(withError error: AuthError){
        ActivitySpinner.hide()
        self.delegate?.AuthFailed(withError: error)
    }
    
}
