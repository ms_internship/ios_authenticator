//
//  BasePhoneVerificationProtocol.swift
//  AuthenticatorModule
//
//  Created by LinuxPlus on 9/18/17.
//  Copyright © 2017 Allam. All rights reserved.
//

import Foundation

protocol BasePhoneVerificationProtocol {
    var authenticator: PhoneAuthProtocol? {get set}
    func verify()
}
