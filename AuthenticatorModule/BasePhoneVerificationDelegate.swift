
//
//  BasePhoneVerificationDelegate.swift
//  AuthenticatorModule
//
//  Created by LinuxPlus on 9/18/17.
//  Copyright © 2017 Allam. All rights reserved.
//

import Foundation

protocol BasePhoneVerificationDelegate {
    func phoneVerificationSuccessfull(withMessage message: String)
    func phoneVerificationFailed(withError error: String)
}
