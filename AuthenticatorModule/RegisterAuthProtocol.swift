//
//  RegisterAuthProtocol.swift
//  AuthenticatorModule
//
//  Created by LinuxPlus on 9/18/17.
//  Copyright © 2017 Allam. All rights reserved.
//

import Foundation

protocol RegisterAuthProtocol {
    func createUserByEmailAndPassword(email: String, password: String, completion:@escaping (String?, AuthError?) -> Void)
}
