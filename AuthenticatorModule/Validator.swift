//
//  Validator.swift
//  AuthenticatorModule
//
//  Created by LinuxPlus on 9/14/17.
//  Copyright © 2017 Allam. All rights reserved.
//

import UIKit
import Foundation

class Validator: ValidationProtocol{
    
    func emailValidation(emailTF: UITextField) -> String?{
        let email = emailTF.text!
        if email == ""{
            return "Email is not valid"
        }
        return nil
    }
    
    func passwordValidation(passwordTF: UITextField) -> String?{
        let password = passwordTF.text!
        if password.characters.count < 6{
            return "Password must be at least 6 characters"
        }
        return nil
    }
    
    func confirmPasswordValidation(passwordTF: UITextField, confirmTF: UITextField) -> String?{
        
        if passwordTF.text! != confirmTF.text!{
            return "Confirm password doesn't match"
        }
       return nil
    }


    func setError(to textField: UITextField){
        textField.layer.borderWidth = 1
        textField.layer.cornerRadius = 5
        textField.layer.borderColor = UIColor.red.cgColor
        textField.becomeFirstResponder()
    }

    
    
}
