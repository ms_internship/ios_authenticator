//
//  LoginAuthProtocol.swift
//  AuthenticatorModule
//
//  Created by LinuxPlus on 9/9/17.
//  Copyright © 2017 Allam. All rights reserved.
//

import Foundation

protocol LoginAuthProtocol{
    
    var delegate: AuthenticatorDelegate?{get set}
    
    func authByEmailAndPassword(email: String, password: String, completion: @escaping (String?, AuthError?) -> Void)
    func authByGmail(idToken:String, accessToken: String,  completion: @escaping (String?, AuthError?) -> Void)
    func authByFacebook(accessToken: String,  completion: @escaping (String?, AuthError?) -> Void)
}

extension LoginAuthProtocol{
//    func authByEmailAndPassword(email: String, password: String){}
//    func authByGmail(idToken:String, accessToken: String){}
//    func authByFacebook(accessToken: String){}
}
